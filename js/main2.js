// * Напишіть функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією,
// повертаючи новий масив.

let any_array = ['Вася', 'Гриша', null, 1, 4, 66, undefined, , , 'Жанна', , 'Аристарх'];

document.write(`<h3> ${any_array}`);

console.log(any_array);



function someSort(arry) {
  let new_arr = [...arry];    // ну или any_array.slice(); 
  for (let i = 0; i < new_arr.length; i++) {
    if ((typeof new_arr[i]) === 'string') {
      new_arr[i] = new_arr[i].toUpperCase();
    }
  }
  return new_arr;
}

function someSort1(arry) {
  let new_arr = [...arry];    // ну или any_array.slice(); 
  for (let i = 0; i < new_arr.length; i++) {
    if ((typeof new_arr[i]) === 'number') {
      new_arr[i] = new_arr[i] * new_arr[i];
    }
  }
  return new_arr;
}

function someSort2(arry) {
  let new_arr = [...arry];    // ну или any_array.slice();   
  for (let i = new_arr.length - 1; i >= 0; i--) {
    if (new_arr[i] === undefined || new_arr[i] === null) {
      new_arr.splice([i], 1);
    }
  }
  return new_arr;
}

function mapP(fn, arr) {
  return fn(arr);
}

document.write(`<h3> ${mapP(someSort, any_array)}`);

console.log(mapP(someSort, any_array));







// * Перепишіть функцію за допомогою оператора '?' або '||'
// Наступна функція повертає true, якщо параметр age більше 18. В іншому випадку вона ставить запитання confirm і повертає його результат.
// function checkAge(age) {
// if (age > 18) {
// return true;
// } else {
// return confirm('Батьки дозволили?');
// } }


let client_age = parseInt(prompt('Введите свой возраст'));

function checkAge(age) {
  return (age > 18) ? true : confirm('Есть разрешение родителей?');
}

console.log(checkAge(client_age));